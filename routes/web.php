<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\IFrameController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\WebsiteController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', function () {
    return view('welcome');
})->name('index');

//Redirect from / to /index for better navigation handling
Route::get('/', function () {
    return redirect()->route('index');
})->name('landing');

Route::get('index/website-detail/{website_id}', [WebsiteController::class, 'websiteDetail'])->name('website-detail');

//Authenticated user routes
Route::middleware(['auth', 'verified'])->group(function () {
    Route::view('/update-password', 'auth.passwords.update')->name('auth.passwords.update');


    Route::post('/iframe-send', [WebsiteController::class, 'recieveJsonFromIframe'])->name('iframe-send');
    Route::get('index/website-chart/{website_id}', [WebsiteController::class, 'websiteChart'])->name('website-chart');

    Route::get('favorite-websites', [HomeController::class, 'favoriteWebsites'])->name('favorite-websites');
    Route::get('login-redirects',  [HomeController::class, 'redirect']);
    Route::get('/dashboard', [HomeController::class, 'index'])->name('home');
    Route::get('profile', [HomeController::class, 'profile'])->name('profile');
    Route::post('/logout', [LoginController::class, 'destroy'])->name('logout');

    //Admin routes
    Route::get('admin-home', [AdminController::class, 'index'])->name('admin-home');
    Route::get('manage-users',  [AdminController::class, 'manageUsers'])->name('manage-users');
    Route::get('manage-websites',  [AdminController::class, 'manageWebsites'])->name('manage-websites');
});
