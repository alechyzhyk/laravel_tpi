<?php
/**
 * File with custom platform configuration
 */
return [

    // Max rating which can be given to a website. Min value 1, max value 255
    'max-rating' => 10,

];
