<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code_alpha',
        'flag_url'
    ];

    /**
    * Visitors list
    */
    public function visitors()
    {
        return $this->hasMany(Visitor::class);
    }

    /**
    * Languages list
    */
    public function languages()
    {
        return $this->belongsToMany(Language::class)->as('country_language');
    }
}
