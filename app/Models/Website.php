<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Website extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image_url',
        'only_auth',
    ];


   /**
    * Evaluations list.
    */
   public function evaluations()
   {
       return $this->hasMany(Evaluation::class);
   }

   /**
    * Users who added as favorite list.
    */
   public function favorites()
   {
       return $this->belongsToMany(User::class, 'favorites')->as('favorites')->withTimestamps();
   }

   /**
    * Related subcategory
    */
    public function subcategory()
    {
        return $this->belongsTo(Subcategory::class);
    }

    /**
    * Website translations list
    */
    public function languages()
    {
        return $this->belongsToMany(Language::class, 'website_translation')->as('website_translation')->withPivot('title','description');
    }
}
