<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ratingux',
        'ratingui',
    ];

    /**
     * User relation.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Website relation.
     */
    public function website()
    {
        return $this->belongsTo(Website::class);
    }
}
