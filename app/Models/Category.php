<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;


    /**
    * Related subcategories list.
    */
    public function subcategories()
    {
        return $this->hasMany(Subcategory::class);
    }

    /**
    * Category translations list
    */
    public function languages()
    {
        return $this->belongsToMany(Language::class, 'category_translation')->as('category_translation')->withPivot('title');
    }
}
