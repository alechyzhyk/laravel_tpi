<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
    ];

    /**
    * Countries list
    */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
    * Countries list
    */
    public function countries()
    {
        return $this->belongsToMany(Country::class);
    }

    /**
    * Category translations list
    */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_translation')->as('category_translation')->withPivot('title');
    }

    /**
    * Subcategory translations list
    */
    public function subcategories()
    {
        return $this->belongsToMany(Subcategory::class, 'subcategory_translation')->as('subcategory_translation')->withPivot('title');
    }


    /**
    * Website translations list
    */
    public function websites()
    {
        return $this->belongsToMany(Website::class, 'website_translation')->as('website_translation')->withPivot('title','description');
    }
}
