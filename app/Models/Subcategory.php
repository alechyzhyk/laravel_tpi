<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    use HasFactory;

    /**
    * Related websites list.
    */
    public function websites()
    {
        return $this->hasMany(Website::class);
    }

    /**
    * Related category.
    */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
    * Subcategory translations list
    */
    public function languages()
    {
        return $this->belongsToMany(Language::class,'subcategory_translation')->as('subcategory_translation')->withPivot('title');
    }
}
