<?php
/**
 * Controls dashboard and other user pages
 */

namespace App\Http\Controllers;

use App\Models\Language;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user= Auth::user();
        $lang = Language::select('id')->where('code','=',session()->get('locale'))->get()[0];
        $langId=$lang->id;
        $evaluations = $user->evaluations->groupBy('website_id');
        $websites=[];
        // For every evaluation get the linked website
        foreach($evaluations as $evaluation){
            array_push($websites, $evaluation[0]->website()
            ->with(['subcategory.category.languages'=>function($query) use(&$langId){
                return $query->where('language_id', '=', $langId);
            }])->with(['subcategory.languages'=>function($query) use(&$langId){
                return $query->where('language_id', '=', $langId);
            }])->with('languages', function($query) use(&$langId){
                return $query->where('language_id', '=', $langId);
            })->with('evaluations')->get()[0]->toArray());
        }

        //Remap all data for easier usage
        foreach($websites as $key=>$website){
            $websites[$key]['id']=$website['id'];
            $websites[$key]['image_url']=$website['image_url'];
            $websites[$key]['title']=$website['languages'][0]['website_translation']['title'];
            $websites[$key]['subcategory']=$website['subcategory']['languages'][0]['subcategory_translation']['title'];
            $websites[$key]['category']=$website['subcategory']['category']['languages'][0]['category_translation']['title'];
            foreach($website['evaluations'] as $evalkey=>$evaluation){
                $websites[$key]['evaluations']['ratingui'][$evalkey]=$evaluation['ratingui'];
                $websites[$key]['evaluations']['ratingui'][$evalkey]=round(array_sum($websites[$key]['evaluations']['ratingui'])/count($websites[$key]['evaluations']['ratingui']), 2);
                $websites[$key]['evaluations']['ratingux'][$evalkey]=$evaluation['ratingux'];
                $websites[$key]['evaluations']['ratingux'][$evalkey]=round(array_sum($websites[$key]['evaluations']['ratingux'])/count($websites[$key]['evaluations']['ratingux']), 2);
                $websites[$key]['evaluations']['created_at'][$evalkey]=date('d-m-Y', strtotime($evaluation['created_at']));
            }
        }
        return view('dashboard', compact('websites'));
    }

    /**
     * Show the application profile form.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $user= Auth::user();
        return view('profile', compact('user'));
    }

    /**
     * Show bookmarks.
     *
     * @return \Illuminate\Http\Response
     */
    public function favoriteWebsites()
    {
        $lang = Language::select('id')->where('code','=',session()->get('locale'))->get()[0];
        $langId=$lang->id;
        $websites = Auth::user()->favorites()
        ->with(['subcategory.category.languages'=>function($query) use(&$langId){
            return $query->where('language_id', '=', $langId);
        }])->with(['subcategory.languages'=>function($query) use(&$langId){
            return $query->where('language_id', '=', $langId);
        }])->with('languages', function($query) use(&$langId){
            return $query->where('language_id', '=', $langId);
        })->paginate(6);
        return view('favorite-websites', compact('websites'));
    }

    /**
     * Redirects to admin or normal dashboard based on user privileges.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirect()
    {
        if(Auth::user()->is_admin){
            return redirect()->route('admin-home');
        }
        return redirect()->route('home');
    }
}


