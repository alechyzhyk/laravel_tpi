<?php
/**
 * Controls routes and views for admin pages
 */

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
     /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Auth::user()->is_admin){
            abort(404);
        }
        $countries=Country::all();
        $data['countries'] = [];
        //Get countries list
        foreach($countries as $key=>$country){
            $data['countries'][$key]['visitorNum'] = count($country->visitors()->get()->toArray());
            $data['countries'][$key]['visitsTotal'] = $country->visitors->sum('visits_number');
            $data['countries'][$key]['name'] = $country->name;
            $data['countries'][$key]['flag_url'] = $country->flag_url;
            $data['countries'][$key]['code_alpha'] = $country->code_alpha;
        }
        return view('admin/dash-admin', compact('data'));
    }

    /**
     * Show the CRUD user management.
     *
     * @return \Illuminate\Http\Response
     */
    public function manageUsers()
    {
        if(!Auth::user()->is_admin){
            abort(404);
        }
        return view('admin/manage-users');
    }

    /**
     * Show the CRUD website management.
     *
     * @return \Illuminate\Http\Response
     */
    public function manageWebsites()
    {
        if(!Auth::user()->is_admin){
            abort(404);
        }
        return view('admin/manage-websites');
    }
}
