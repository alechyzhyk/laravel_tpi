<?php

/**
 * Chyzhyk Aleh
 * v1.3
 * Overrides logout, used to store language in new session after logout.
 * 03.06.2021
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    /**
    * Log the user out of the application.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function destroy(Request $request)
    {
        //Fixes session locale being lost when logging out
        $locale =  session()->get('locale');

        Auth::guard()->logout();
        $request->session()->regenerateToken();

        session()->put('locale', $locale);

        return redirect()->route('index');
    }
}
