<?php
/**
 * Controls website and chart detail page
 */

namespace App\Http\Controllers;

use App\Models\Evaluation;
use App\Models\Language;
use App\Models\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class WebsiteController extends Controller
{

    /**
     * Show the website detail page.
     *
     * @return \Illuminate\Http\Response
     */
    public function websiteDetail($website_id)
    {
        $langId = Language::select('id')->where('code','=',session()->get('locale'))->get('id')->toArray()[0]['id'];

        //Get website from its id with languages and categories
        $website = Website::where('id', '=', $website_id)
        ->with(['subcategory.category.languages'=>function($query) use(&$langId){
            return $query->where('language_id', '=', $langId);
        }])->with(['subcategory.languages'=>function($query) use(&$langId){
            return $query->where('language_id', '=', $langId);
        }])->with('languages', function($query) use(&$langId){
            return $query->where('language_id', '=', $langId);
        })->get()[0];

        $data['website']=$website;
        $data['max_rating']=config('custom.max-rating');

        //Based on previous url determine where to return on button "return" press
        if(str_ends_with(URL::previous(),'dashboard')){
            $data['parent'] = 'home';
        }
        elseif(str_ends_with(URL::previous(),'favorite-websites')){
            $data['parent']='favorite-websites';
        }
        else{
            $data['parent'] ='index';
        }
        //Get translations
        $data['translations']['send_button'] = __('websites.send_eval');
        $data['translations']['ux_label'] = __('websites.ratingux_label');
        $data['translations']['ui_label'] = __('websites.ratingui_label');
        $data['translations']['error'] = __('websites.msg_error');
        //Check if user is authenticated to display eval form
        $data['auth'] = Auth::user() ? true : false;
        //Defines if user has to create/update evaluation
        $data['evaluation'] = false;
        if(Auth::user() && !empty(Evaluation::where('user_id', '=', Auth::user()->id)->where('website_id', '=', $website_id)->get()->toArray())){
            $data['evaluation'] = Evaluation::where('user_id', '=', Auth::user()->id)->where('website_id', '=', $website_id)->get()[0]->toArray();
            $data['translations']['send_button'] = __('websites.update_eval');
        }
        return view('website-detail', compact('data'));
    }

    /**
     * Show the website chart detail page.
     *
     * @return \Illuminate\Http\Response
     */
    public function websiteChart($website_id)
    {
        $langId = Language::select('id')->where('code','=',session()->get('locale'))->get('id')->toArray()[0]['id'];

        $website = Website::where('id', '=', $website_id)
        ->with(['subcategory.category.languages'=>function($query) use(&$langId){
            return $query->where('language_id', '=', $langId);
        }])->with(['subcategory.languages'=>function($query) use(&$langId){
            return $query->where('language_id', '=', $langId);
        }])->with('languages', function($query) use(&$langId){
            return $query->where('language_id', '=', $langId);
        })->with('evaluations')->get()[0]->toArray();

        $data['website']['id']=$website['id'];
        $data['website']['image_url']=$website['image_url'];
        $data['website']['title']=$website['languages'][0]['website_translation']['title'];
        $data['website']['subcategory']=$website['subcategory']['languages'][0]['subcategory_translation']['title'];
        $data['website']['category']=$website['subcategory']['category']['languages'][0]['category_translation']['title'];
        $data['translations']['chart_title'] = __('websites.evaluations');

        foreach($website['evaluations'] as $evalkey=>$evaluation){
            $data['website']['evaluations']['ratingui'][$evalkey]=$evaluation['ratingui'];
            $data['website']['evaluations']['ratingui'][$evalkey]=round(array_sum($data['website']['evaluations']['ratingui'])/count($data['website']['evaluations']['ratingui']), 2);
            $data['website']['evaluations']['ratingux'][$evalkey]=$evaluation['ratingux'];
            $data['website']['evaluations']['ratingux'][$evalkey]=round(array_sum($data['website']['evaluations']['ratingux'])/count($data['website']['evaluations']['ratingux']), 2);
            $data['website']['evaluations']['created_at'][$evalkey]=date('d-m-Y', strtotime($evaluation['created_at']));
        }

        return view('website-chart', compact('data'));
    }


    /**
     * Function recieves data from blade template
     * (iframe->blade->controller->database) and stores into database.
     *
     * @return \Illuminate\Http\Response
     */
    public function recieveJsonFromIframe(Request $request)
    {
        //Checks if values of ratings are correct
        if(!(Auth::user() && $request->ratingux > 0 && $request->ratingux < config('custom.max-rating') && $request->ratingui > 0 && $request->ratinguî < config('custom.max-rating'))){
            return response()->json([
                'success' => 'true',
                'markup' => '<div class="bg-red-400 rounded shadow-sm p-5 my-5 w-full"><p class="text-white">'.__('websites.msg_error').'</p></div>',
            ]);
        }
        //Checks if user is authenticated and already evaluated website
        if(!empty(Evaluation::where('user_id', '=', Auth::user()->id)->where('website_id', '=', $request->web_id)->get()->toArray())){
            Evaluation::where('user_id', '=', Auth::user()->id)->where('website_id', '=', $request->web_id)->get()[0]->delete();
        }
        $evaluation=new Evaluation([
            'ratingux' => $request->ratingux,
            'ratingui' => $request->ratingui,
        ]);
        $evaluation->user()->associate(Auth::user());
        $evaluation->website()->associate(Website::find($request->web_id));
        $evaluation->save();
        return response()->json([
            'success' => 'true',
            'markup' => '<div class="bg-green-400 rounded shadow-sm p-5 my-5 w-full"><p class="text-white">'.__('websites.msg_success').'</p></div>',
        ]);
    }

}
