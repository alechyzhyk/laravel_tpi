<?php
/**
 * Gathers visitor information before accessing website
 */

namespace App\Http\Middleware;

use App\Models\Country;
use App\Models\Language;
use App\Models\Visitor;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class Localization
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //By default select fallback language
        $locale=config('app.fallback_locale');
        //If user is connected, apply configuration language
        if(Auth::user()){
            $locale=Auth::user()->language->code;
            //Set locale into session and apply language for the website
            session()->put('locale',$locale);
            App::setLocale($locale);
        }
        else{

            //If user isn't connected, check if session already has language.
            if(session()->get('locale')==null){
                $prefered = $request->getPreferredLanguage();
                foreach(Language::all() as $key=>$lang){
                    if($prefered==$lang->code){
                        $locale=$prefered;
                        break;
                    }
                    if($prefered.'_'.geoip()->getLocation()['iso_code']==$lang->code){
                        $locale=$prefered.'_'.geoip()->getLocation()['iso_code'];
                        break;
                    }
                }
                //Set locale into session and apply language for the website
                session()->put('locale',$locale);
                App::setLocale($locale);
            }
            else{
                App::setLocale(session()->get('locale'));
            }
        }
        //Check if visitor is already known and is in session
        if(session()->get('visitor_saved')==null){
            // Check visitors country. If didn't find entry in database, then apply 'Other' country
            $countryExists=false;
            foreach(Country::all() as $country){
                if(geoip()->getLocation()['iso_code'] == $country->code_alpha){
                    $savedCountry=$country;
                    $countryExists=true;
                    break;
                }
            }
            if(!$countryExists){
                $savedCountry=Country::where('code_alpha','=', '00')->get()[0];
            }

            //Check if visitor already exists in DB. Increments visits number or create a new one
            //based on result
            $visExists=false;
            foreach(Visitor::all() as $visitor){
                if(geoip()->getLocation()['ip'] == $visitor->ip_address){
                    $visitor->visits_number += 1;
                    $visitor->save();
                    $visExists=true;
                    break;
                }
            }
            if(!$visExists){
                $visitor = new Visitor([
                    'ip_address' => geoip()->getLocation()['ip'],
                    'visits_number' => 1
                ]);
                $visitor->country()->associate($savedCountry);
                $visitor->save();
            }
            session()->put('visitor_saved', true);
        }
        return $next($request);
    }
}
