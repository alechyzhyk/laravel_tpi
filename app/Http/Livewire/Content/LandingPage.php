<?php
/**
 * Controls langing page websites gallery
 */

namespace App\Http\Livewire\Content;

use App\Models\Category;
use App\Models\Language;
use App\Models\Subcategory;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class LandingPage extends Component
{
    use WithPagination;

    public $subCatId;
    public $subcategories=[];
    public $categoryId;
    private $cards=[];

    /**
     * Render landing page gallery view.
     *
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        $lang = Language::select('id')->where('code','=',session()->get('locale'))->get()[0];
        $langId=$lang->id;
        $categories=$lang->categories;

        // On first page access show first subcategory of first category.
        if($this->categoryId==null){
            $this->categoryId=$categories[0]->id;
            $this->subCatId=$categories[0]->subcategories()->get()[0]->id;
        }

        //Get subcategories with translations.
        $category=Category::find($this->categoryId);
        $this->subcategories=$category->subcategories()->with('languages',
            function($query) use(&$langId){
                return $query->where('language_id', '=',  $langId);
            })->get();

        //Get websites with translations, subcategories and categories.
        $subcategory=Subcategory::find($this->subCatId);
        if(Auth::user()){
            $this->websites = $subcategory->websites()
            ->with(['subcategory.category.languages'=>function($query) use(&$langId){
                return $query->where('language_id', '=', $langId);
            }])->with(['subcategory.languages'=>function($query) use(&$langId){
                return $query->where('language_id', '=', $langId);
            }])->with('languages', function($query) use(&$langId){
                return $query->where('language_id', '=', $langId);
            })->with('evaluations')
            ->orderBy('created_at', 'desc')->paginate(6);
        }
        else{
            $this->websites = $subcategory->websites()->where('only_auth', '=', false)
            ->with(['subcategory.category.languages'=>function($query) use(&$langId){
                return $query->where('language_id', '=', $langId);
            }])->with(['subcategory.languages'=>function($query) use(&$langId){
                return $query->where('language_id', '=', $langId);
            }])->with('languages', function($query) use(&$langId){
                return $query->where('language_id', '=', $langId);
            })->with('evaluations')
            ->orderBy('created_at', 'desc')->paginate(6);
        }


        // Reset pagination.
        $this->resetPage();

        return view('livewire.content.landing-page',['websites' => $this->websites, 'categories' => $categories])
            ->extends('components.layout')
            ->section('content');
    }

    /**
     * Get categories based on their type id.
     */
    public function getSubategoriesFromCategory()
    {
        $this->subCatId=Category::find($this->categoryId)->subcategories()->get()[0]->id;
    }

    /**
     * Override default pagination view.
     */
    public function paginationView()
    {
        return 'partials.pagination';
    }
}
