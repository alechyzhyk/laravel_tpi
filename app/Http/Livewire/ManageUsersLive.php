<?php
/**
 * Manages users in admin CRUD page
 */

namespace App\Http\Livewire;

use App\Models\Language;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class ManageUsersLive extends Component
{

    public $users, $languages, $langCode, $lastname, $firstname, $email, $birthdate, $password, $user_id, $lang_id, $curr_lang_id;
    public $isModalOpen = 0;

    /**
     * Renders crud user view.
     *
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        $this->users = User::with('language')->orderBy('id')->get();
        $this->languages = Language::all();
        return view('livewire.manage-users-live');
    }

    /**
     * Create new user
     */
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }

    /**
     * Open modal with create form
     */
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }

    /**
     * Close modal with create form
     */
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }

    /**
     * Reset inputs
     */
    private function resetCreateForm(){
        $this->lastname = '';
        $this->firstname = '';
        $this->birthdate = '';
        $this->email = '';
        $this->password = '';
        $this->user_id = '';
        $this->lang_id = 1;
    }

    /**
     * Validate and store user into database
     */
    public function store()
    {
        $this->validate([
            'lastname' => ['required', 'string', 'max:255'],
            'firstname' => ['required', 'string', 'max:255'],
            'birthdate' => ['required', 'date'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
            ],
        ]);

        if($this->user_id==null){
            $user=new User([
                'lastname' => $this->lastname,
                'firstname' => $this->firstname,
                'birthdate' => $this->birthdate,
                'is_admin' => 0,
                'email' => $this->email,
                'password' => Hash::make($this->password),
            ]);
            $user->language()->associate(Language::find($this->lang_id));
            $user->save();
        }
        else{
            $user = User::find($this->user_id);
            $user->lastname = $this->lastname;
            $user->firstname =  $this->firstname;
            $user->birthdate = $this->birthdate;
            $user->email = $this->email;
            if($this->password!=null){
                $user->password = Hash::make($this->password);
            }
            $user->language()->associate(Language::find($this->lang_id));
            $user->push();
        }
        session()->flash('message', $this->user_id ? 'User updated.' : 'User created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }

    /**
     * Edit user
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        $this->user_id = $id;
        $this->lastname = $user->lastname;
        $this->firstname = $user->firstname;
        $this->birthdate = $user->birthdate;
        $this->email = $user->email;
        $this->lang_id = $user->language->id;

        $this->openModalPopover();
    }

    /**
     * Delete user
     */
    public function delete($id)
    {
        User::find($id)->delete();
        session()->flash('message', 'User deleted.');
    }
}
