<?php
/**
 * Manages websites in admin CRUD page
 */

namespace App\Http\Livewire;

use App\Models\Category;
use Livewire\Component;
use App\Models\Language;
use App\Models\Subcategory;
use App\Models\Website;
use Livewire\WithFileUploads;

class ManageWebsitesLive extends Component
{
    use WithFileUploads;

    public $image, $subCatdId, $catId, $websiteId, $only_auth;
    public $categories, $subcategories;
    public $languages;
    public $isOpen = 0;
    public $types;
    public $langId;

    /**
     * Mounts variables before accessing page
     */
    public function mount()
    {
        $this->langId = Language::select('id')->where('code','=',session()->get('locale'))->get('id')->toArray()[0]['id'];
        $langId=$this->langId;

        $this->categories=Category::with(['languages'=>function($query) use(&$langId){
            return $query->where('language_id', '=', $langId);
        }])->get();
    }

    /**
     * Renders crud website view.
     *
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        $langId=$this->langId;

        // Get websites list
        $websites = Website::with(['subcategory.category.languages'=>function($query) use(&$langId){
            return $query->where('language_id', '=', $langId);
        }])->with(['subcategory.languages'=>function($query) use(&$langId){
            return $query->where('language_id', '=', $langId);
        }])->with('languages', function($query) use(&$langId){
            return $query->where('language_id', '=', $langId);
        })->orderBy('created_at', 'desc')->paginate(20);

        foreach(Website::all() as $website){
            foreach($website->languages as $lang){
                $translations[$lang->code][$website->id]=$lang->website_translation;
            }
        }

        // Manage Categories and subcategories inside dropdowns
        if($this->catId==null){
            $this->catId=$this->categories[0]->id;
            $this->subCatId=Category::find($this->catId)->subcategories()->get()[0]->id;
        }

        $this->subcategories=Category::find($this->catId)->subcategories()->with(['languages'=>function($query) use(&$langId){
            return $query->where('language_id', '=', $langId);
        }])->get();

        return view('livewire.manage-websites-live')
            ->withWebsites($websites)
            ->withTranslations($translations);
    }

    /**
     * Get subcategories based on their category
     */
    public function getSubCatFromCategory()
    {
        $this->subCatdId=Category::find($this->catId)->subcategories()->get()[0]->id;
    }

    /**
     * Create new website
     */
    public function create()
    {
        $this->resetInputFields();
        $this->openModal();
    }

    /**
     * Open modal with create form
     */
    public function openModal()
    {
        $this->isOpen = true;
    }

    /**
     * Close modal with create form
     */
    public function closeModal()
    {
        $this->isOpen = false;
    }

    /**
     * Reset inputs
     */
    private function resetInputFields(){
        $this->catId = '';
        $this->subCatdId = '';
        $this->only_auth = '';
        $this->image = '';
        $this->websiteId='';
    }

    /**
     * Validate and store website into database
     */
    public function store()
    {
        if(!$this->websiteId){
            $this->validate([
                'image' => 'required|image|mimes:jpg,jpeg,png,svg,gif|max:2048',
            ]);
            $website=new Website();
        }
        else{
            $website=Website::find($this->websiteId);
            foreach($this->languages as $key=>$data){
                $language=Language::where('code','=',$key)->get()[0];
                $website->languages()->detach();
                $website->save();
            }
        }

        if($this->image!=null){
            $website->image_url='storage/'.$this->image->store('/images/galerie', ['disk' => 'public']);
        }

        $website->only_auth = $this->only_auth;
        $website->subcategory()->associate(Subcategory::find($this->subCatdId));
        $website->save();

        foreach($this->languages as $key=>$data){
            $language=Language::where('code','=',$key)->get()[0];
            $website->languages()->attach($language->id, ['title' => $data['title'], 'description' => $data['description']]);
            $website->save();
        }

        session()->flash('message', $this->websiteId ? 'Website updated successfully.' : 'Website created successfully.');
        $this->closeModal();
        $this->resetInputFields();
    }

    /**
     * Edit website
     */
    public function edit($id)
    {
        $website = Website::findOrFail($id);
        $this->catId = $website->subcategory->category->id;
        $this->subCatdId = $website->subcategory->id;
        $this->only_auth = $website->only_auth;
        foreach($website->languages as $lang){
            $this->languages[$lang->code]['title']=$lang->website_translation->title;
            $this->languages[$lang->code]['description']=$lang->website_translation->description;
        }
        $this->websiteId = $id;
        $this->openModal();
    }

    /**
     * Delete website
     */
    public function delete($id)
    {
        $this->websiteId = $id;
        Website::find($id)->delete();
        session()->flash('message', 'Website deleted successfully.');
    }
}
