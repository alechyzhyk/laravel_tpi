<?php
/**
 * Controls bookmarks managament (Add or remove)
 */

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class Favorites extends Component
{
    public $website;
    public $user;
    public $favorites;
    public $user_id;

    /**
     * Render bookmarks view.
     *
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        $this->user=Auth::user();
        $this->favorites=$this->user->favorites()->get();
        foreach($this->favorites as $favorite){
            if($favorite->id == $this->website->id){
                return view('livewire.favorites')
                ->withExists(true);
            }
        }
        return view('livewire.favorites')
                ->withExists(false);
    }

    /**
     * Adds website to favorites
     */
    public function addToFavorites()
    {
        $exists=false;
        foreach($this->favorites as $favorite){
            if($favorite->id == $this->website->id){
               $exists=true;
               break;
            }
        }
        if(!$exists){
            $this->user->favorites()->attach($this->website->id);
        }
    }

    /**
     * Removes website from favorites
     */
    public function removeFromFavorites()
    {
        foreach( $this->favorites as $favorite){
            if($favorite->id == $this->website->id){
                $this->user->favorites()->detach($this->website->id);
                break;
            }
        }
    }
}
