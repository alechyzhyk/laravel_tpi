<?php
/**
 * Selects language and applies for session or database
 */

namespace App\Http\Livewire;

use App\Models\Language;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class LanguageSelector extends Component
{
    public $lang;
    public $langs;

    //Listener for page refresh
    protected $listeners = ['refreshPage'];

    /**
     * Render dropdown view.
     *
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        if($this->lang==null){
            $this->lang=session('locale');
        }
        $this->langs = Language::with(['countries'])->get();
        return view('livewire.language-selector');
    }

    /**
     * Refresh current page when selected language.
     */
    public function refreshPage($selectedLangcode)
    {
        foreach(Language::all() as $langObject){
            if($langObject->code==$selectedLangcode){
                session()->put('locale',$selectedLangcode);
                App::setLocale($this->lang);
                if(Auth::user()){
                    $user = Auth::user();
                    $user->language()->associate($langObject);
                    $user->push();
                }
                return redirect()->to(url()->previous());
            }
        }
    }
}
