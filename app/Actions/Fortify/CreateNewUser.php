<?php

namespace App\Actions\Fortify;

use App\Models\Language;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'lastname' => ['required', 'string', 'max:255'],
            'firstname' => ['required', 'string', 'max:255'],
            'birthdate' => ['required', 'date'],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class),
            ],
            'password' => $this->passwordRules(),
        ])->validate();

        $user=new User([
            'lastname' => $input['lastname'],
            'firstname' => $input['firstname'],
            'birthdate' => $input['birthdate'],
            'is_admin' => 0,
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);

        $lang = Language::select('id')->where('code','=',session()->get('locale'))->get()[0];
        $user->language()->associate($lang);
        $user->save();
        return $user;
    }
}
