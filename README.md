# Website evaluation platform

Website evaluation platform is a Laravel based internaltional project for managing and evaluatig web creations.

## Installation

1. Use the package manager [composer](https://getcomposer.org/) to install the project.

```bash
composer install
```

2. Make a copy of [.env.example](./.env.example)  file and name it .env

3. Configure your host, database information and mailer data using .env file

4. Generate application key using

```bash
php aritsan key:generate
```

5. Migrate tables and seed your database

```bash
php aritsan migrate
php aritsan db:seed
```

5. Run virtual server using 

```bash
php aritsan serve
```


## Version history

<b>v1.3 :</b> 

Minor fixes;

Added README file

<b>v1.2 :</b> 

Fixed translations for chart and iframe in PDF file;

Fixed language reset after logout;

<b>v1.1 :</b> 

Fixed title in PDF file;

Removed unused class usage;

Fixed default visit number in Localization middleware;

Fixed error occuring when creating user while without changing value in language dropdown

Fixed evaluation error

<b>v1.0 :</b> 

First stable version of project


