<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WebsiteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($subcat=1; $subcat<=6; $subcat++){
            for($web=0; $web<2; $web++){
                DB::table('websites')->insert([
                    'subcategory_id' => $subcat,
                    'image_url' => '/images/websites/template.png',
                    'only_auth' => 0,
                    'created_at' => now(),
                    'updated_at' => now(),
                ]);
            }
        }
        for($web=0; $web<5; $web++){
            DB::table('websites')->insert([
                'subcategory_id' => 1,
                'image_url' => '/images/websites/template.png',
                'only_auth' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
