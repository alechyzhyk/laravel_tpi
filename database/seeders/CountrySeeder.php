<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            'name' => 'France',
            'code_alpha' => 'FR',
            'flag_url' => '/images/flags/FR.png'
        ]);
        DB::table('countries')->insert([
            'name' => 'United States of America',
            'code_alpha' => 'US',
            'flag_url' => '/images/flags/US.png'
        ]);
        DB::table('countries')->insert([
            'name' => 'Switzerland',
            'code_alpha' => 'CH',
            'flag_url' => '/images/flags/CH.png'
        ]);
        DB::table('countries')->insert([
            'name' => 'Canada',
            'code_alpha' => 'CA',
            'flag_url' => '/images/flags/CA.png'
        ]);
        DB::table('countries')->insert([
            'name' => 'Other',
            'code_alpha' => '00',
            'flag_url' => '/images/flags/template.png'
        ]);
    }
}
