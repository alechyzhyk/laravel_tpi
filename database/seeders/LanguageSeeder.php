<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            'name' => 'Français',
            'code' => 'fr_CH',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('languages')->insert([
            'name' => 'Français',
            'code' => 'fr_CA',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('languages')->insert([
            'name' => 'Français',
            'code' => 'fr_FR',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('languages')->insert([
            'name' => 'English',
            'code' => 'en_US',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        DB::table('languages')->insert([
            'name' => 'English',
            'code' => 'en_CA',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('country_language')->insert([
            'language_id' => '1',
            'country_id' => '3',
        ]);
        DB::table('country_language')->insert([
            'language_id' => '2',
            'country_id' => '4',
        ]);
        DB::table('country_language')->insert([
            'language_id' => '3',
            'country_id' => '1',
        ]);
        DB::table('country_language')->insert([
            'language_id' => '4',
            'country_id' => '2',
        ]);
        DB::table('country_language')->insert([
            'language_id' => '4',
            'country_id' => '5',
        ]);
        DB::table('country_language')->insert([
            'language_id' => '5',
            'country_id' => '4',
        ]);
    }
}
