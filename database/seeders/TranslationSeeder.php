<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TranslationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($categorie=1; $categorie<=3; $categorie++){
            DB::table('category_translation')->insert([
                'language_id' => '1',
                'category_id' => $categorie,
                'title' => 'FR_CH Category '.$categorie,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::table('category_translation')->insert([
                'language_id' => '2',
                'category_id' => $categorie,
                'title' => 'FR_CA Category '.$categorie,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::table('category_translation')->insert([
                'language_id' => '3',
                'category_id' => $categorie,
                'title' => 'FR_FR Category '.$categorie,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::table('category_translation')->insert([
                'language_id' => '4',
                'category_id' => $categorie,
                'title' => 'EN_US Category '.$categorie,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::table('category_translation')->insert([
                'language_id' => '5',
                'category_id' => $categorie,
                'title' => 'EN_CA Category '.$categorie,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
        for($subcategorie=1; $subcategorie<=6; $subcategorie++){
            DB::table('subcategory_translation')->insert([
                'language_id' => '1',
                'subcategory_id' => $subcategorie,
                'title' => 'FR_CH Subcategory '.$subcategorie,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::table('subcategory_translation')->insert([
                'language_id' => '2',
                'subcategory_id' => $subcategorie,
                'title' => 'FR_CA Subcategory '.$subcategorie,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::table('subcategory_translation')->insert([
                'language_id' => '3',
                'subcategory_id' => $subcategorie,
                'title' => 'FR_FR Subcategory '.$subcategorie,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::table('subcategory_translation')->insert([
                'language_id' => '4',
                'subcategory_id' => $subcategorie,
                'title' => 'EN_US Subcategory '.$subcategorie,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::table('subcategory_translation')->insert([
                'language_id' => '5',
                'subcategory_id' => $subcategorie,
                'title' => 'EN_CA Subcategory '.$subcategorie,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
        for($website=1; $website<=17; $website++){
            DB::table('website_translation')->insert([
                'language_id' => '1',
                'website_id' => $website,
                'title' => 'FR_CH Website '.$website,
                'description' => 'FR_CH Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::table('website_translation')->insert([
                'language_id' => '2',
                'website_id' => $website,
                'title' => 'FR_CA Website '.$website,
                'description' => 'FR_CA Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::table('website_translation')->insert([
                'language_id' => '3',
                'website_id' => $website,
                'title' => 'FR_FR Website '.$website,
                'description' => 'FR_FR Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::table('website_translation')->insert([
                'language_id' => '4',
                'website_id' => $website,
                'title' => 'EN_US Website '.$website,
                'description' => 'EN_US Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'created_at' => now(),
                'updated_at' => now(),
            ]);
            DB::table('website_translation')->insert([
                'language_id' => '5',
                'website_id' => $website,
                'title' => 'EN_CA Website '.$website,
                'description' => 'EN_CA Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }
}
