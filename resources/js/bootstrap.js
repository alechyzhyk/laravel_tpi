window._ = require('lodash');
window.jQuery = require('jquery');

window.ApexCharts = require('apexcharts');
import ApexCharts from 'apexcharts';

//Partie ajouté pour jsPDF
window.jspdf = require('jspdf');
window.jsPDF = window.jspdf.jsPDF;
/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
