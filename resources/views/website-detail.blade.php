{{-- Chyzhyk Aleh
20.05.2021
Website details page content --}}

@extends('components.layout')

@section('title')
    {!! __('page_title.website') !!}
@endsection

@section('content')
    {{-- Change return path based on previous page --}}
    <a
    href={{ route($data['parent']) }}
    class="mx-auto md:mx-10 mt-10 block w-1/4 md:w-auto md:inline-flex shadow-lg text-center px-7 py-2 text-white transition-colors duration-150 bg-gradient-to-r from-green-400 to-blue-400 hover:from-pink-500 rounded focus:shadow-outline">{!! __('websites.return') !!}</a>

    <!-- component -->
    <div class="flex flex-wrap md:m-20 m-4">
        <div class="lg:w-1/2 w-full mx-auto flex flex-col">
            <div class="md:aspect-w-16 md:aspect-h-16 xl:aspect-w-16 xl:aspect-h-12 aspect-w-12 aspect-h-12 mb-10">
                <iframe id="img-iframe" data-img-url="{{ URL::asset($data['website']->image_url) }}" data-send-url="{{ route('iframe-send') }}" class="w-full h-full" src="/html/iframe.html" title="product-detail"></iframe>
                @csrf
            </div>
            <button onclick="maximize()" id="maximize" class="mx-auto bg-gray-100 rounded p-3 text-xl md:mb-0 shadow-lg focus:shadow-sm focus:outline-none hover:bg-gray-200 border-0 py-2 px-6 ">{!! __('websites.maximize') !!}</button>
            <button onclick="minimize()" id="minimize" class="fixed z-50 right-5 top-5 bg-gray-100 rounded p-3 text-xl md:mb-0 shadow-lg focus:shadow-sm focus:outline-none hover:bg-gray-200 border-0 py-2 px-6 mx-auto hidden">X</button>
            <div id="section-appendable"></div>
        </div>
        <div class="lg:w-1/2 w-full mx-auto flex flex-col mb-20">
            <div class="w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0 mb-8">
                <h2 class="text-lg inline title-font text-gray-900">{{ $data['website']->subcategory->category->languages[0]->category_translation->title }}</h2>&nbsp;-
                <h2 class="text-sm inline title-font text-gray-500 tracking-widest">{{  $data['website']->subcategory->languages[0]->subcategory_translation->title}}</h2>
                <h1 class="text-gray-900 text-3xl title-font font-medium mb-1">{{ $data['website']->languages[0]->website_translation->title}}</h1>
                <p class="leading-relaxed">{{ $data['website']->languages[0]->website_translation->description}}</h1>
            </div>
            {{-- Will be used in the future for favorites --}}
            @auth
                @livewire('favorites', ['website' => $data['website']])
            @endauth
        </div>
    </div>

@endsection

@section('page-scripts')
<script>
    /*
    * Sends image to iframe
    */
    document.getElementById('img-iframe').onload = function() {
        var frame = document.getElementById('img-iframe');
        var data = {imageUrl: frame.getAttribute('data-img-url'), auth : @json($data['auth']), translations: @json($data['translations'])  }
        if (@json($data['evaluation'])){
            data.evaluation = @json($data['evaluation']);
        }
        frame.contentWindow.postMessage(JSON.stringify(data), '*');
    };

    /*
    * Recieves data from iframe and sends it to controller
    */
    window.addEventListener('message', function(event) {
        var error_text = @json($data['translations']).error;
        if(event.data!=""){
            const data = JSON.parse(event.data);
            const url = document.getElementById('img-iframe').getAttribute('data-send-url');
            axios.post(`${url}`, {
                ratingui:  data['ratingui'],
                ratingux: data['ratingux'],
                web_id: @json($data['website']->id)
            })
            .then(function (response) {
                document.getElementById("section-appendable").innerHTML += response['data']['markup'];
            })
            .catch(function (error) {
                document.getElementById("section-appendable").innerHTML += '<div class="bg-red-400 rounded shadow-sm p-5 my-5 w-full"><p class="text-white">' + error_text + '</p></div>';
            });
        }
    });
</script>

<script>
    function maximize() {
        const iframe = document.querySelector('#img-iframe')
        iframe.classList.add('z-40');
        iframe.classList.add('p-3');
        iframe.classList.add('fixed');
        iframe.classList.add('inset-0');
        iframe.classList.add('border');
        iframe.classList.add('bg-gray-400');

        iframe.parentElement.classList.remove('md:aspect-w-16');
        iframe.parentElement.classList.remove('md:aspect-h-16');
        iframe.parentElement.classList.remove('xl:aspect-w-16');
        iframe.parentElement.classList.remove('xl:aspect-h-12');

        const minimize = document.querySelector('#minimize');
        minimize.classList.remove('hidden');
    }

    function minimize() {
        const iframe = document.querySelector('#img-iframe')
        iframe.classList.remove('z-50');
        iframe.classList.remove('p-3');
        iframe.classList.remove('fixed');
        iframe.classList.remove('inset-0');
        iframe.classList.remove('border');
        iframe.classList.remove('bg-gray-400');

        iframe.parentElement.classList.add('md:aspect-w-16');
        iframe.parentElement.classList.add('md:aspect-h-16');
        iframe.parentElement.classList.add('xl:aspect-w-16');
        iframe.parentElement.classList.add('xl:aspect-h-12');

        const minimize = document.querySelector('#minimize');
        minimize.classList.add('hidden');
    }
</script>
@endsection
