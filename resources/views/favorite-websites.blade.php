{{-- Chyzhyk Aleh
    20.05.2021
    Bookmarks websites list
--}}

@extends('components.layout')

@section('title')
    {!! __('page_title.bookmarks') !!}
@endsection

@section('content')
    <div class="container px-5 py-24 mx-auto">
        <div class="flex md:flex-row flex-col mb-8 ">
            <div class="grid lg:grid-cols-3 md:grid-cols-2 grid-cols-1">
                @if (isset($websites) && $websites->isNotEmpty())
                    @foreach ($websites as $website)
                        @include('partials.landing.website-card')
                    @endforeach
                @else
                    <p class="text-xl m-auto lg:col-span-3 md:col-span-2 col-span-1 mt-10">{!! __('validation.no_element') !!}</p>
                @endif
            </div>
        </div>
        <div class="mt-10 flex items-center justify-center">
            {{ $websites->links() }}
        </div>
    </div>
@endsection
