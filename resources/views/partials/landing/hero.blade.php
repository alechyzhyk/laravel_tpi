{{-- Chyzhyk Aleh
20.05.2021
Landing page hero content --}}

<div class="text-gray-600 body-font">
  <div class="container mx-auto flex px-5 py-24 md:flex-row flex-col items-center">
      <div class="lg:flex-grow md:w-1/2 lg:pr-24 md:pr-16 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
            <h1 class="title-font sm:text-4xl text-3xl mb-4 font-medium text-gray-900">{!! __('landing.title') !!}</h1>
            <p class="mb-10 leading-relaxed">{!! __('landing.description') !!}</p>
            <div class="w-full">
              @guest
              <a href={{ route('login') }} class="my-4 inline-flex shadow-lg items-center px-10 py-2 text-white transition-colors duration-150 bg-gradient-to-r from-green-400 to-blue-400 hover:from-pink-500 rounded focus:shadow-outline">{!! __('nav.login') !!}</a>
              <a href={{ route('register') }} class="my-4 inline-flex shadow-lg items-center px-10 py-2 ml-2 text-white transition-colors duration-150 bg-gradient-to-r from-green-400 to-blue-400 hover:from-pink-500 rounded focus:shadow-outline">{!! __('nav.register') !!}</a>
              @endguest
              @auth
              <a href={{ route('home') }} class="my-4 inline-flex shadow-lg items-center px-10 py-2 text-white transition-colors duration-150 bg-gradient-to-r from-green-400 to-blue-400 hover:from-pink-500 rounded focus:shadow-outline">{!! __('nav.dashboard') !!}</a>
              {{-- <a href={{ route('dashboard') }} class="inline-flex shadow-lg items-center px-10 py-2 ml-2 text-white transition-colors duration-150 bg-gradient-to-r from-green-400 to-blue-400 hover:from-pink-500 rounded focus:shadow-outline">Dashboard</a> --}}
              <form method="POST" class="my-4 cursor-pointer inline-flex shadow-lg items-center px-10 py-2 ml-2 text-white transition-colors duration-150 bg-gradient-to-r from-green-400 to-blue-400 hover:from-pink-500 rounded focus:shadow-outline" action="{{ route('logout') }}">
                @csrf
                <button type="submit" class="items-center text-white" href={{route('logout')}}">
                    {!! __('nav.logout') !!}
                </button>
              </form>
              @endauth
            </div>
      </div>
      <div class="lg:w-1/4 md:w-2/3 w-5/6">
            <img class="object-cover object-center rounded" alt="hero" src="images/hero/lp.svg">
      </div>
  </div>
</div>
