{{-- Chyzhyk Aleh
20.05.2021
Content of a single element in gallery --}}

<div class="max-w-2xl md:w-11/12 px-8 py-4 bg-white rounded-lg shadow-md dark:bg-gray-800 mx-auto mb-8">
    <div class="flex items-center justify-between mt-4">
        <div class="flex items-center">
            <div class="flex flex-col">
                <a class="font-bold text-gray-700 cursor-pointer dark:text-gray-200">{{ $website->subcategory->category->languages[0]->category_translation->title }}</a>
                <span class="text-sm font-light text-gray-600 dark:text-gray-400">{{  $website->subcategory->languages[0]->subcategory_translation->title  }}</span>
            </div>
        </div>
    </div>
    <div class="mt-2 w-auto">
        <a href="{{ route('website-detail', $website->id) }}" id={{ 'website-'.$website->id }} class="text-2xl font-bold text-gray-700 dark:text-white hover:text-gray-600 dark:hover:text-gray-200 hover:underline">{{ $website->languages[0]->website_translation->title }}</a>
        <img class="lg:h-48 md:h-36 w-full object-cover object-center" src="{{ URL::asset($website->image_url) }}" alt="produit">
        <p class="mt-2 text-gray-600 dark:text-gray-300">{!! \Illuminate\Support\Str::limit($website->languages[0]->website_translation->description, $limit = 255) !!}</p>
    </div>

    <div class="flex items-center  flex-wrap mt-4">
        <a href="{{ route('website-detail', $website->id) }}" class="text-indigo-500 inline-flex items-center md:mb-2 lg:mb-0">{!! __('websites.detail_button') !!}
            <svg class="w-4 h-4 ml-2" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round">
                <path d="M5 12h14"></path>
                <path d="M12 5l7 7-7 7"></path>
            </svg>
        </a>
    </div>
</div>
