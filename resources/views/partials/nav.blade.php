{{-- Chyzhyk Aleh
20.05.2021
Website navigation --}}

<nav class="bg-white shadow">
    <div x-data="{ isOpen: false }" class="mx-auto max-w-7xl py-3 px-6 md:px-0 md:flex md:justify-between md:items-center">
        <div class="inline-block justify-between items-center h-8">
            {{-- Mobile menu button --}}
            <div class="flex md:hidden">
                <button
                    type="button"
                    class="text-gray-500 hover:text-gray-600 focus:outline-none focus:text-gray-600"
                    aria-label="toggle menu"
                    @click="isOpen = !isOpen">
                    <img class="h-6 w-6" src="{{ URL::asset('images/menu_lp_strip/hamburger.svg') }}">
                </button>
            </div>
        </div>
        @livewire('language-selector')
        {{-- Menu, if mobile set to hidden --}}
        <div :class="isOpen ? 'show' : 'hidden'" class="md:flex items-center md:block mt-4 md:mt-0">
            <div class="flex flex-col md:flex-row items-left">
                <a class="{{ Request::is('index*') ? 'md:border-b border-gray-500' : '' }} text-center md:w-full w-auto my-1 text-sm text-gray-700 font-medium hover:text-indigo-500 md:mx-4 md:my-0" href={{route('index')}}>{!! __('nav.index') !!}</a>
                @guest
                    <a class="{{ Request::is('login*') ? 'md:border-b border-gray-500' : '' }} text-center md:w-full w-auto my-1 text-sm text-gray-700 font-medium hover:text-indigo-500 md:mx-4 md:my-0" href={{route('login')}}>{!! __('nav.login') !!}</a>
                    <a class="{{ Request::is('register*') ? 'md:border-b border-gray-500' : '' }} text-center md:w-full w-auto my-1 text-sm text-gray-700 font-medium hover:text-indigo-500 md:mx-4 md:my-0" href={{route('register')}}>{!! __('nav.register') !!}</a>
                @endguest
                @auth
                    @if ( Auth::user()->is_admin)
                        <a class="{{ Request::is('admin*') ? 'md:border-b border-gray-500' : '' }} text-center md:w-full w-auto my-1 text-sm text-gray-700 font-medium hover:text-indigo-500 md:mx-4 md:my-0" href={{route('admin-home')}}>{!! __('nav.dashboard_adm') !!}</a>
                        <a class="{{ Request::is('manage-users*') ? 'md:border-b border-gray-500' : '' }} text-center md:w-full w-auto my-1 text-sm text-gray-700 font-medium hover:text-indigo-500 md:mx-4 md:my-0" href={{route('manage-users')}}>{!! __('nav.manage_users') !!}</a>
                        <a class="{{ Request::is('manage-websites*') ? 'md:border-b border-gray-500' : '' }} text-center md:w-full w-auto my-1 text-sm text-gray-700 font-medium hover:text-indigo-500 md:mx-4 md:my-0" href={{route('manage-websites')}}>{!! __('nav.manage_websites') !!}</a>
                    @endif
                    <a class="{{ Request::is('favorite*') ? 'md:border-b border-gray-500' : '' }} text-center md:w-full w-auto my-1 text-sm text-gray-700 font-medium hover:text-indigo-500 md:mx-4 md:my-0" href={{route('favorite-websites')}}>{!! __('nav.bookmarks') !!}</a>
                    <a class="{{ Request::is('dashboard*') ? 'md:border-b border-gray-500' : '' }} text-center md:w-full w-auto my-1 text-sm text-gray-700 font-medium hover:text-indigo-500 md:mx-4 md:my-0" href={{route('home')}}>{!! __('nav.dashboard') !!}</a>
                    <a class="{{ Request::is('profile*') ? 'md:border-b border-gray-500' : '' }} text-center md:w-full w-auto my-1 text-sm text-gray-700 font-medium hover:text-indigo-500 md:mx-4 md:my-0" href={{route('profile')}}>{!! __('nav.manage_profile') !!}</a>
                    <form method="POST" class="text-center md:w-full w-auto my-1 text-sm text-gray-700 font-medium hover:text-indigo-500 md:mx-4 md:my-0" action="{{ route('logout') }}">
                        @csrf
                        <button type="submit" class="text-center md:w-full w-auto my-1 text-sm font-bold text-gray-700 hover:text-indigo-500 md:mx-4 md:my-0" href={{route('logout')}}">
                            {!! __('nav.logout') !!}
                        </button>
                    </form>
                @endauth
            </div>
        </div>
    </div>
</nav>
