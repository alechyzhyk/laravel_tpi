{{-- Chyzhyk Aleh
20.05.2021
Language drop down --}}

<div class=" ml-5 md:ml-0 inline-block items-right">
    <select id="lang-select" name="lang" wire:change="refreshPage()" wire:model="lang" class="mb-8 rounded select-lang border-gray-100 shadow-lg inline">
        @foreach ($langs as $key=>$langObj)
            <option data-imagesrc="{{ URL::asset($langObj->countries[0]->flag_url)}}" wire:key={{$key}}
                value={{ $langObj->code }} {{ $langObj->code == $lang ? 'selected="selected"' : '' }}>{{ $langObj->name }}</option>
        @endforeach
    </select>
    <script>
        //Used to disable trigger change first time
        let selCounter=false;
        jQuery('#lang-select').ddslick({
            width:"150px",
            onSelected: function (data) {
                if(selCounter){
                    Livewire.emit('refreshPage', data['selectedData']['value']);
                }
                selCounter=true;
            }
        });
    </script>
</div>

