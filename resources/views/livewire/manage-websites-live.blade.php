{{-- Chyzhyk Aleh
20.05.2021
Crud livewire website manager --}}
<div>
    <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
        @if (session()->has('message'))
            <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3" role="alert">
                <div class="flex">
                <div>
                    <p class="text-sm">{{ session('message') }}</p>
                </div>
                </div>
            </div>
        @endif
        <button wire:click="create()" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded my-3">{!! __('admin.new_website_label') !!}</button>
        {{-- Open create form when called --}}
        @if($isOpen)
            @include('livewire.modal.create-website')
        @endif
        <table class="table-fixed w-full">
            <thead>
                <tr class="bg-gray-100">
                    <th class="px-4 py-2 w-20">{!! __('admin.id_label') !!}</th>
                    <th class="px-4 py-2">{!! __('admin.category_label') !!}</th>
                    <th class="px-4 py-2">{!! __('admin.subcategory_label') !!}</th>
                    @foreach ($translations as $key=>$translation)
                        <th class="px-4 py-2">{!! __('admin.title_label') !!} {{$key}}</th>
                    @endforeach
                    <th class="px-4 py-2">{!! __('admin.autorization_label') !!}</th>
                    <th class="px-4 py-2">{!! __('admin.image_label') !!}</th>
                    <th class="px-4 py-2">{!! __('admin.actions_label') !!}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($websites as $website)
                    <tr>
                        <td class="border px-4 py-2">{{ $website->id }}</td>
                        <td class="border px-4 py-2">{{ $website->subcategory->category->languages[0]->category_translation->title }}</td>
                        <td class="border px-4 py-2">{{ $website->subcategory->languages[0]->subcategory_translation->title }}</td>
                        @foreach ($translations as $key=>$translation)
                            <td class="border px-4 py-2">{{ $translation[$website->id]->title }}</td>
                        @endforeach
                        <td class="border px-4 py-2">
                            @if ($website->only_auth)
                                {!! __('admin.auth_only') !!}
                            @else
                                {!! __('admin.all') !!}
                            @endif
                        </td>
                        <td class="border px-4 py-2">
                            <img class="object-cover object-center rounded border border-gray-200 mb-8" src="{{ URL::asset($website->image_url) }}">
                        </td>
                        <td class="border px-4 py-2">
                        <button wire:click="edit({{ $website->id }})" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">{!! __('admin.edit_label') !!}</button>
                            <button wire:click="delete({{ $website->id }})" class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">{!! __('admin.delete_label') !!}</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
