{{-- Chyzhyk Aleh
20.05.2021
Gallery content --}}

<div class="container px-5 py-24 mx-auto">
    <div class="flex w-full sm:w-1/2 mx-auto md:flex-row flex-col mb-8">
        @if (isset($categories) && !empty($categories))
            <div class="mr-5 md:w-1/2 w-full mb-10">
                <select name="category" wire:change="getSubategoriesFromCategory()" wire:model="categoryId" class="w-full rounded text-xl border-gray-100 border shadow-sm">
                    @foreach ($categories as $key=>$category)
                        <option id={{'option-category'.$category->id}} wire:key={{ $key }} value={{$category->id}}>{{$category->category_translation->title}}</option>
                    @endforeach
                </select>
            </div>
        @endif
        @if (isset($subcategories) && !empty($subcategories))
            <div class="md:w-1/2 w-full mb-10">
                <select name="subcategory" wire:model="subCatId" class="w-full rounded text-xl border-gray-100 border shadow-sm">
                    @foreach ($subcategories as $key=>$subcategory)
                        <option id={{'option-category'.$subcategory->id}} value={{$subcategory->id}}>{{$subcategory->languages[0]->subcategory_translation->title}}</option>
                    @endforeach
                </select>
            </div>
        @endif
    </div>
    <div class="mx-auto w-full text-center text-3xl" wire:loading.delay >
        Chargement...
    </div>
    <div wire:loading.remove class="grid lg:grid-cols-3 md:grid-cols-2 grid-cols-1">
        @empty ($websites)
            <p class="text-xl m-auto lg:col-span-3 md:col-span-2 col-span-1 mt-10">{{ __('landing.no_element')}}</p>
        @else
            @foreach ($websites as $website)
                @if ($website->only_auth)
                    @auth
                        @include('partials.landing.website-card')
                    @endauth
                @else
                    @include('partials.landing.website-card')
                @endif
            @endforeach
        @endempty
    </div>
    <div class="mt-10 flex items-center justify-center">
        {{ $websites->links() }}
    </div>
</div>
