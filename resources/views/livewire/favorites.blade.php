{{-- Chyzhyk Aleh
20.05.2021
Add/remove bookmark buttons for a website --}}

<div>
    <button class="mx-auto md:mx-10 w-2/3 md:w-auto block md:inline-flex shadow-lg items-center px-10 py-2 text-white transition-colors duration-150 bg-gradient-to-r from-green-400 to-blue-400 hover:from-pink-500 rounded focus:shadow-outline"
        @if ($exists)
        wire:click='removeFromFavorites()'>{!! __('websites.remove_favorite') !!}
        @else
        wire:click='addToFavorites()'>{!! __('websites.add_favorite') !!}
        @endif
    </button>
</div>
