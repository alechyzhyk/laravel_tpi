{{-- Chyzhyk Aleh
20.05.2021
Crud livewire user manager --}}

<div class="bg-white overflow-hidden shadow-xl sm:rounded-lg px-4 py-4">
    @if (session()->has('message'))
    <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3"
        role="alert">
        <div class="flex">
            <div>
                <p class="text-sm">{{ session('message') }}</p>
            </div>
        </div>
    </div>
    @endif
    <button wire:click="create()"
        class="bg-green-700 text-white font-bold py-2 px-4 rounded my-3">{!! __('admin.new_user_label') !!}</button>
    {{-- Open create form when called --}}
    @if($isModalOpen)
        @include('livewire.modal.create-user')
    @endif
    <table class="table-auto w-full">
        <thead>
            <tr class="bg-gray-100">
                <th class="px-4 py-2 w-10">{!! __('admin.id_label') !!}</th>
                <th class="px-4 py-2">{!! __('admin.role_label') !!}</th>
                <th class="px-4 py-2">{!! __('admin.lastname_label') !!}</th>
                <th class="px-4 py-2">{!! __('admin.firstname_label') !!}</th>
                <th class="px-4 py-2">{!! __('admin.language_label') !!}</th>
                <th class="px-4 py-2 w-1/4">{!! __('auth.email_label') !!}</th>
                <th class="px-4 py-2">{!! __('admin.birthdate_label') !!}</th>
                <th class="px-4 py-2">{!! __('admin.actions_label') !!}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr>
                <td class="border px-4 py-2">{{ $user->id }}</td>
                <td class="border px-4 py-2">
                    @if ( $user->is_admin   )
                        {!! __('admin.admin_text') !!}
                    @else
                        {!! __('admin.standard_text') !!}
                    @endif</td>
                <td class="border px-4 py-2">{{ $user->lastname }}</td>
                <td class="border px-4 py-2">{{ $user->firstname}}</td>
                <td class="border px-4 py-2">{{ $user->language->name}} ({{ $user->language->code }}) </td>
                <td class="border px-4 py-2">{{ $user->email}}</td>
                <td class="border px-4 py-2">{{ $user->birthdate}}</td>
                <td class="border px-4 py-2">
                    <button wire:click="edit({{ $user->id }})"
                        class="bg-blue-500  text-white font-bold py-2 px-4 rounded">{!! __('admin.edit_label') !!}</button>
                    <button wire:click="delete({{ $user->id }})"
                        class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">{!! __('admin.delete_label') !!}</button>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
