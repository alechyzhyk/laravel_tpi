{{-- Chyzhyk Aleh
20.05.2021
Modal for website management form --}}

<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>

        <!-- This element is to trick the browser into centering the modal contents. -->
        <span class="hidden sm:inline-block sm:align-middle sm:h-screen"></span>​
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full" role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>

                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <div class="mb-4">
                            <label for="image" class="block text-gray-700 text-sm font-bold mb-2">{!! __('admin.image_label') !!}:</label>
                            <input type="file" class="appearance-none text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="image" wire:model="image">
                            @error('image') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="category" class="block text-gray-700 text-sm font-bold mb-2">{!! __('admin.category_label') !!}:</label>
                            <select id='category' name="category" wire:change="getSubCatFromCategory()"  wire:model="catId" class="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                                @foreach ($categories as $category_option)
                                    <option value={{ $category_option->id }}>{{ $category_option->languages[0]->category_translation->title }}</option>
                                @endforeach
                            </select>
                            @error('catId') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="subcategory" class="block text-gray-700 text-sm font-bold mb-2">{!! __('admin.subcategory_label') !!}:</label>
                            <select id='subcategory' name="subcategory" wire:model="subCatdId" class="shadow border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                                @foreach ($subcategories as $subcategory_option)
                                    <option value={{ $subcategory_option->id }}>{{ $subcategory_option->languages[0]->subcategory_translation->title }}</option>
                                @endforeach
                            </select>
                            @error('subCatdId') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="only_auth" class="block text-gray-700 text-sm font-bold mb-2">{!! __('admin.autorization_label') !!}:</label>
                            <input type="radio" name="only_auth" value="0" wire:model="only_auth">
                                {!! __('admin.all') !!}
                            <input type="radio" name="only_auth" value="1"  wire:model="only_auth">
                                {!! __('admin.auth_only') !!}
                            @error('only_auth') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        @foreach ($translations as $key=>$value)
                            <div class="mb-4">
                                <label for="title-{{ $key }}" class="block text-gray-700 text-sm font-bold mb-2">{!! __('admin.title_label') !!} {{ $key }}:</label>
                                <input type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="title-{{ $key }}" placeholder="Title" wire:model="languages.{{ $key }}.title">
                                @error('languages') <span class="text-red-500">{{ $message }}</span>@enderror
                            </div>
                            <div class="mb-4">
                                <label for="description-{{ $key }}" class="block text-gray-700 text-sm font-bold mb-2">{!! __('admin.description_label') !!} {{ $key }}:</label>
                                <textarea type="text" class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="description-{{ $key }}" placeholder="Description" wire:model="languages.{{ $key }}.description"></textarea>
                                @error('languages.{{ $key }}.description') <span class="text-red-500">{{ $message }}</span>@enderror
                            </div>
                        @endforeach
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button wire:click.prevent="store()" type="button" class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-green-600 text-base leading-6 font-medium text-white shadow-sm hover:bg-green-500 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            {!! __('admin.save_label') !!}
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModal()" type="button" class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-medium text-gray-700 shadow-sm hover:text-gray-500 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            {!! __('admin.cancel_label') !!}
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
