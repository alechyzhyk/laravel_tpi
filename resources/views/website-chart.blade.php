{{-- Chyzhyk Aleh
20.05.2021
Chart details page content --}}

@extends('components.layout')

@section('title')
    {!! __('page_title.chart') !!}
@endsection

@section('content')
    <!-- component -->
    <div class="align-center w-4/5 m-auto">
        <div id="line-chart-{{ $data['website']['id'] }}" class="w-full mt-5 border">
            <apexchart type="line" :options="chartOptions" :series="series"></apexchart>
        </div>
        <a href={{ route('home') }} class="my-10 ml-5 sm:ml-10 inline-flex shadow-lg items-center px-10 py-2 text-white transition-colors duration-150 bg-gradient-to-r from-green-400 to-blue-400 hover:from-pink-500 rounded focus:shadow-outline">{{ __('websites.return')}}</a>
        <button class="ml-5 sm:ml-10 my-10 p-2 bg-green-700 text-white shadow" onclick="downloadPDFVersion()">
            {{ __('websites.download_pdf')}}
        </button>
    </div>
@endsection

@section('page-scripts')
<script>
    //Chart generation
    var website = @json($data['website']);
    var translations = @json($data['translations']);
    var options = {
        series: [
        {
            name: "Rating UX",
            data: website['evaluations']['ratingux']
        },
        {
            name: "Rating UI",
            data: website['evaluations']['ratingui']
        },
        ],
        chart: {
            type: 'line',
            toolbar: {
                show: true,
            },
            zoom: {
                enabled: true,
            }
        },
        dataLabels: {
            enabled: true
        },
        stroke: {
            curve: 'straight'
        },
        title: {
            text: translations['chart_title'],
            align: 'left'
        },
        grid: {
            row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
            opacity: 0.5
            },
        },
        xaxis: {
            categories: website['evaluations']['created_at'],
            labels: {
            show: true,
            }
        }
    };

    var chart = new ApexCharts(document.querySelector("#line-chart-" + website['id']), options);
    chart.render();

    //PDF file generation
    function downloadPDFVersion() {
        var website = @json($data['website']);
        chart.dataURI().then(({ imgURI, blob }) => {
            var pdf = new jsPDF({
                orientation: "landscape",
            });
            var width = pdf.internal.pageSize.getWidth();
            var height = pdf.internal.pageSize.getHeight();
            pdf.addImage(imgURI, 'PNG', 0, 0, width, height);
            pdf.text(website['title'],120,7);
            pdf.save("download.pdf");
        })
    }
</script>

@endsection
