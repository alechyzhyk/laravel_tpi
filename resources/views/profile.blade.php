{{-- Chyzhyk Aleh
    20.05.2021
    Update profile information form
--}}

@extends('components.layout')

@section('title')
    {!! __('page_title.profile') !!}
@endsection

@section('content')
<div class="sm:container sm:mx-auto sm:max-w-lg sm:mt-10">
    <div class="flex">
        <div class="w-full">
            <div class="flex flex-col break-words bg-white sm:border-1 sm:rounded-md sm:shadow-sm sm:shadow-lg">
                <div class="font-semibold bg-gray-200 text-gray-700 py-5 px-6 sm:py-6 sm:px-8 sm:rounded-t-md">
                    {!! __('profile.update_title') !!}
                </div>
                <form class="w-full px-6 space-y-6 sm:px-10 sm:space-y-8" method="POST"
                    action="{{ route('user-profile-information.update') }}">
                    @method('PUT')
                    @csrf
                    <div class="flex flex-wrap">
                        <label for="lastname" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {{ __('auth.lastname_label') }}:
                        </label>
                        <input id="lastname" type="text" class="form-input border w-full @error('lastname')  border-red-500 @enderror"
                            name="lastname" value="{{ $user->lastname }}" autocomplete="lastname" required autofocus>
                        @error('lastname')
                        <p class="text-red-500 text-xs italic mt-4">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>
                    <div class="flex flex-wrap">
                        <label for="firstname" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {{ __('auth.firstname_label') }}:
                        </label>
                        <input id="firstname" type="text" class="form-input border w-full @error('firstname')  border-red-500 @enderror"
                            name="firstname" value="{{ $user->firstname }}" autocomplete="firstname" required autofocus>
                        @error('firstname')
                        <p class="text-red-500 text-xs italic mt-4">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>
                    <div class="flex flex-wrap">
                        <label for="email" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {{ __('auth.email_label') }}:
                        </label>

                        <input id="email" type="email"
                            class="form-input border w-full @error('email') border-red-500 @enderror" name="email"
                            value="{{ $user->email }}" required autocomplete="email">

                        @error('email')
                        <p class="text-red-500 text-xs italic mt-4">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>
                    <div class="flex flex-wrap">
                        <label for="birthdate" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {{ __('auth.birthdate_label') }}:
                        </label>
                        <input id="birthdate" type="date"
                            class="form-input border w-full @error('birthdate') border-red-500 @enderror" name="birthdate"
                            value="{{ $user->birthdate }}"" required autocomplete="birthdate">
                        @error('birthdate')
                        <p class="text-red-500 text-xs italic mt-4">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>

                    <div class="flex flex-wrap">
                        <button type="submit"
                            class="my-6 w-full select-none font-bold whitespace-no-wrap p-3 rounded-lg text-base leading-normal no-underline text-gray-100 bg-blue-500 hover:bg-blue-700 sm:py-4">
                            {!! __('profile.update_label') !!}
                        </button>
                    </div>
                </form>
            </div>
            <a class="mx-auto md:mx-10 mt-10 block w-2/4 md:w-auto md:inline-flex shadow-lg text-center px-7 py-2 text-white transition-colors duration-150 bg-gradient-to-r from-green-400 to-blue-400 hover:from-pink-500 rounded focus:shadow-outline" href="{{ route('auth.passwords.update') }}"">{!! __('profile.change_pwd_label') !!}</a>
        </div>
    </div>
</div>
@endsection
