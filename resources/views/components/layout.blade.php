{{-- Chyzhyk Aleh
20.05.2021
Layout template --}}
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @section('head')
            @include('partials.head')
        @show
    </head>
    <body class="font-sans antialiased">
        <header>
            @section('navbar')
                @include('partials.nav')
            @show
        </header>
        <div class="min-h-screen">
            {{-- Page Content --}}
            <main>
                <section class="text-gray-600 body-font">
                    <div class="mx-auto">
                        <h1 class="mt-10 mb-5 text-center title-font sm:text-3xl text-2xl mb-4 font-medium text-gray-900">
                            @section('title')
                                @yield('title')
                            @show
                        </h1>
                    </div>
                    @section('content')
                        @yield('content')
                    @show
                </section>
            </main>
        </div>
            @section('footer')
                @include('partials.footer')
            @show
        @stack('modals')

        @livewireScripts
    </body>
    @section('page-scripts')
        @yield('page-scripts')
    @show
</html>
