@extends('components.layout')
{{-- Chyzhyk Aleh
19.05.2021
Email verification form --}}

@section('title')
    {!! __('page_title.confirmation') !!}
@endsection

@section('content')
<div class="sm:container sm:mx-auto sm:max-w-lg sm:mt-10">
    <div class="flex">
        <div class="w-full">
            <div class="flex flex-col break-words bg-white sm:border-1 sm:rounded-md sm:shadow-sm sm:shadow-lg">
                <div class="font-semibold bg-gray-200 text-gray-700 py-5 px-6 sm:py-6 sm:px-8 sm:rounded-t-md">
                    {{ __('auth.email_verify_label') }}
                </div>
                <div class="w-full flex flex-wrap text-gray-700 leading-normal text-sm p-6 space-y-4 sm:text-base sm:space-y-6">
                    <p>
                        {{ __('auth.email_verify_text') }}
                    </p>

                    {{-- Resend email --}}
                    <p>
                        {{ __('auth.email_verify_recieve_text') }} <a
                            class="text-blue-500 hover:text-blue-700 no-underline hover:underline cursor-pointer"
                            onclick="event.preventDefault(); document.getElementById('resend-verification-form').submit();">{{ __('auth.email_verify_link') }}</a>.
                    </p>

                    <form id="resend-verification-form" method="POST" action="{{ route( 'verification.send') }}"
                        class="hidden">
                        @csrf
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
