@extends('components.layout')
{{-- Chyzhyk Aleh
20.05.2021
Update password form --}}

@section('title')
    {!! __('page_title.pwd_update') !!}
@endsection

@section('content')
<div class="sm:container sm:mx-auto sm:max-w-lg sm:mt-10">
    <div class="flex">
        <div class="w-full">
            <div class="flex flex-col break-words bg-white sm:border-1 sm:rounded-md sm:shadow-sm sm:shadow-lg">
                <div class="font-semibold bg-gray-200 text-gray-700 py-5 px-6 sm:py-6 sm:px-8 sm:rounded-t-md">
                    {!! __('profile.update_pwd_title') !!}
                </div>
                <form class="w-full px-6 space-y-6 sm:px-10 sm:space-y-8" method="POST"
                    action="{{ route('user-password.update') }}">
                    @csrf
                    @method('PUT')
                    <div class="flex flex-wrap">
                        <label for="current_password" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {!! __('profile.curr_pwd_label') !!}:
                        </label>

                        <input id="current_password" type="password"
                            class="form-input w-full border @error('current_password') border-red-500 @enderror" name="current_password"
                            required autocomplete="current_password">
                        @error('current_password')
                        <p class="text-red-500 text-xs italic mt-4">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>


                    <div class="flex flex-wrap">
                        <label for="password" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {!! __('profile.new_pwd_label') !!}:
                        </label>

                        <input id="password" type="password"
                            class="form-input w-full border @error('password') border-red-500 @enderror" name="password"
                            required autocomplete="new-password">

                        @error('password')
                        <p class="text-red-500 text-xs italic mt-4">
                            {{ $message }}
                        </p>
                        @enderror
                    </div>

                    <div class="flex flex-wrap">
                        <label for="password-confirm" class="block text-gray-700 text-sm font-bold mb-2 sm:mb-4">
                            {!! __('profile.update_pwd_title') !!}:
                        </label>

                        <input id="password-confirm" type="password" class="form-input w-full border"
                            name="password_confirmation" required autocomplete="new-password">
                    </div>

                    <div class="flex flex-wrap">
                        <button type="submit"
                            class="my-6 w-full select-none font-bold whitespace-no-wrap p-3 rounded-lg text-base leading-normal no-underline text-gray-100 bg-blue-500 hover:bg-blue-700 sm:py-4">
                            {!! __('profile.update_pwd_label') !!}
                        </button>
                    </div>
                </form>
            </div>
            <a class="mx-auto md:mx-10 mt-10 block w-1/4 md:w-auto md:inline-flex shadow-lg text-center px-7 py-2 text-white transition-colors duration-150 bg-gradient-to-r from-green-400 to-blue-400 hover:from-pink-500 rounded focus:shadow-outline" href="{{ route('profile') }}"> {!! __('websites.return') !!}</a>
        </div>
    </div>
</div>
@endsection
