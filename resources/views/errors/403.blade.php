{{-- Chyzhyk Aleh
20.05.2021
404 error --}}

@extends('components.layout')

@section('title')

@endsection

@section('content')
    <div class="text-center w-full mt-10">
        <h1 class=" mt-10 title-font sm:text-7xl text-4xl mb-4 font-medium text-gray-900">403 Fobidden</h1>
        <p>Oops! Something is wrong.</p>
    </div>
@endsection
