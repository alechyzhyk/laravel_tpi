{{-- Chyzhyk Aleh
    20.05.2021
    User's dashboard with evaluated websites gallery
--}}

@extends('components.layout')

@section('title')
    {!! __('page_title.dash') !!}
@endsection

@section('content')
<div class="container px-5 py-24 mx-auto">
    <div class="grid lg:grid-cols-3 md:grid-cols-2 grid-cols-1">
        @empty ($websites)
            <p class="text-xl m-auto lg:col-span-3 md:col-span-2 col-span-1 mt-10">Aucun element à afficher</p>
        @else
            @foreach ($websites as $website)
                @include('partials.dashboard.website-card')
            @endforeach
        @endempty
    </div>
</div>
@endsection
