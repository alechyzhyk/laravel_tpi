{{-- Chyzhyk Aleh
    20.05.2021
    Website management page
--}}
@extends('components.layout')

@section('title')
    {!! __('page_title.websites') !!}
@endsection

@section('content')
<div class="container px-5 pb-24 w-full mx-auto flex flex-col mt-10 items-center">
    <div class="flex md:flex-row w-full flex-col mb-8">
        @livewire('manage-websites-live')
    </div>
</div>
@endsection
