{{-- Chyzhyk Aleh
    20.05.2021
    User management page
--}}
@extends('components.layout')

@section('title')
    {!! __('page_title.users') !!}
@endsection

@section('content')
<div class="container px-5 pb-24 mx-auto flex flex-col mt-10 items-center">
    <div class="flex md:flex-row flex-col mb-8">
            @livewire('manage-users-live')
    </div>
</div>
@endsection
