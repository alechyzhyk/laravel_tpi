{{-- Chyzhyk Aleh
    20.05.2021
    Landing page content
--}}

@extends('components.layout')

@section('content')
    @include('partials.landing.hero')
  @livewire('content.landing-page')
@endsection
