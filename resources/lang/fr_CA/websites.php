<?php

return [
    'detail_button' => 'Détails',
    'chart_button' => 'Charte',
    'remove_favorite' => 'Supprimer de favoris',
    'add_favorite' => 'Ajouter aux favoris',
    'maximize' => 'Maximizer',
    'return' => 'Retour',
    'send_button' => 'Envoyer',
    'error_message' => 'Une erreur est survenue, essayez plus tard',
    'succes_message' => 'Succès',
    'download_pdf' => 'Télécharger en PDF',
    'evaluations' => 'Historique des évaluations',
    'update_eval' => 'Modifier',
    'send_eval' => 'Envoyer',
    'ratingui_label' => 'Note UI',
    'ratingux_label' => 'Note UX',
    'msg_error' => 'Erreur, veuillez recharger la page',
    'msg_success' => 'Sucess d\'envoi, les données sont stockées dans la BD',
];
