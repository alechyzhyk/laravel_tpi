<?php

return [
    'dash' => 'Tableau de bord. Liste des sites évalués',
    'dash_admin' => 'Statistique des visites',
    'profile' => 'Votre profil',
    'pwd_update' => 'Votre profil',
    'pwd_reset' => 'Reinitialiser le mot de passe',
    'login' => 'Connectez-vous à votre compte',
    'register' => 'Créer un nouveau compte',
    'bookmarks' => 'Vos favoris',
    'users' => 'Gestion des utilisateurs',
    'websites' => 'Gestion des sites web',
    'confirmation' => 'Confirmation de votre adresse courriel',
    'website' => 'Détails du site web',
    'chart' => 'Détails de la charte',
];
