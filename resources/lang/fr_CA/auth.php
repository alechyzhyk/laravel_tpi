<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '(fr_CA) These credentials do not match our records.',
    'password' => '(fr_CA) The provided password is incorrect.',
    'throttle' => '(fr_CA) Too many login attempts. Please try again in :seconds seconds.',
    'email_label' => '(fr_CA) Email',
    'password_label' => '(fr_CA) Password',
    'password_confirm_label' => '(fr_CA) Confirm password',
    'reset' => '(fr_CA) Reset password',
    'remember' => '(fr_CA) Remeber me',
    'no_account' => "(fr_CA) Don't have an account?",
    'firstname_label' => '(fr_CA) Firstname',
    'lastname_label' => '(fr_CA) Lastname',
    'birthdate_label' => '(fr_CA) Birthdate',
    'have_account' => '(fr_CA) Already have an account?',
    'email_verify_label' =>   '(fr_CA) Verify Your Email Address',
    'email_verify_text' =>   '(fr_CA) Before proceeding, please check your email for a verification link.',
    'email_verify_recieve_text' =>   '(fr_CA) If you did not receive the email,',
    'email_verify_link' =>   '(fr_CA) click here to request another.',
    'email_verify_resend_label' =>   '(fr_CA) A fresh verification link has been sent to your email address.',
];
