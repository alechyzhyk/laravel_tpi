<?php

return [


    'index' => 'Home',
    'login' => 'Login',
    'register' => 'Create account',
    'logout' => 'Logout',
    'stats' => 'Statistics',
    'bookmarks' => 'Bookmarks',
    'dashboard' => 'Dashboard',
    'dashboard_adm' => 'Dashboard Admin',
    'manage_users' => 'User management',
    'manage_websites' => 'Website management',
    'manage_profile' => 'Profile management',
];
