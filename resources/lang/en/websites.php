<?php

return [

    'detail_button' => 'Details',
    'chart_button' => 'Chart',
    'remove_favorite' => 'Remove from bookmarks',
    'add_favorite' => 'Add to bookmarks',
    'maximize' => 'Maximize',
    'return' => 'Back',
    'send_button' => 'Send',
    'error_message' => 'An error occured, please try again later',
    'succes_message' => 'Success',
    'download_pdf' => 'Download PDF',
    'evaluations' => 'Evaluations history',
    'update_eval' => 'Update',
    'send_eval' => 'Submit',
    'ratingui_label' => 'Rating UI',
    'ratingux_label' => 'Rating UX',
    'msg_error' => 'Error, please try again later',
    'msg_success' => 'Success, information stored succesfully',
];
