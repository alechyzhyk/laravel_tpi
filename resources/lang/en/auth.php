<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'email_label' => 'Email',
    'password_label' => 'Password',
    'password_confirm_label' => 'Password',
    'reset' => 'Reset password',
    'remember' => 'Remeber me',
    'no_account' => "Don't have an account?",
    'firstname_label' => 'Firstname',
    'lastname_label' => 'Lastname',
    'birthdate_label' => 'Birthdate',
    'have_account' => 'Already have an account?',
    'email_verify_label' =>   'Verify Your Email Address',
    'email_verify_text' =>   'Before proceeding, please check your email for a verification link.',
    'email_verify_recieve_text' =>   'If you did not receive the email,',
    'email_verify_link' =>   'click here to request another.',
    'email_verify_resend_label' =>   'A fresh verification link has been sent to your email address.',
];
