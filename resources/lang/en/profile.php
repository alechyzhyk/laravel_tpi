<?php

return [
    'update_title' => 'Change profile information',
    'change_pwd_label' => 'Change password',
    'update_label' => 'Update Profile',
    'update_pwd_label' => 'Update password',
    'update_pwd_title' => 'Change password',
    'curr_pwd_label' => 'Current password',
    'new_pwd_label' => 'New password',
    'confirm_pwd_label' => 'Confirm password',
];
