<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '(en_US) These credentials do not match our records.',
    'password' => '(en_US) The provided password is incorrect.',
    'throttle' => '(en_US) Too many login attempts. Please try again in :seconds seconds.',
    'email_label' => '(en_US) Email',
    'password_label' => '(en_US) Password',
    'password_confirm_label' => '(en_US) Confirm password',
    'reset' => '(en_US) Reset password',
    'remember' => '(en_US) Remeber me',
    'no_account' => "(en_US) Don't have an account?",
    'firstname_label' => '(en_US) Firstname',
    'lastname_label' => '(en_US) Lastname',
    'birthdate_label' => '(en_US) Birthdate',
    'have_account' => '(en_US) Already have an account?',
    'email_verify_label' =>   '(en_US) Verify Your Email Address',
    'email_verify_text' =>   '(en_US) Before proceeding, please check your email for a verification link.',
    'email_verify_recieve_text' =>   '(en_US) If you did not receive the email,',
    'email_verify_link' =>   '(en_US) click here to request another.',
    'email_verify_resend_label' =>   '(en_US) A fresh verification link has been sent to your email address.',
];
