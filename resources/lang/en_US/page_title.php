<?php

return [
    'dash' => 'Dashboard. List of evaluated websites',
    'dash_admin' => 'Visitor statistics',
    'profile' => 'Your profile',
    'pwd_update' => 'Your profile',
    'pwd_reset' => 'Reset password',
    'login' => 'Log into your account',
    'register' => 'Create new account',
    'bookmarks' => 'Your bookmarks',
    'users' => 'User management',
    'websites' => 'Website management',
    'confirmation' => 'Email confirmation link',
    'website' => 'Website detail',
    'chart' => 'Chart detail',
];
