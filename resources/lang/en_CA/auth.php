<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '(en_CA) These credentials do not match our records.',
    'password' => '(en_CA) The provided password is incorrect.',
    'throttle' => '(en_CA) Too many login attempts. Please try again in :seconds seconds.',
    'email_label' => '(en_CA) Email',
    'password_label' => '(en_CA) Password',
    'password_confirm_label' => '(en_CA) Confirm password',
    'reset' => '(en_CA) Reset password',
    'remember' => '(en_CA) Remeber me',
    'no_account' => "(en_CA) Don't have an account?",
    'firstname_label' => '(en_CA) Firstname',
    'lastname_label' => '(en_CA) Lastname',
    'birthdate_label' => '(en_CA) Birthdate',
    'have_account' => '(en_CA) Already have an account?',
    'email_verify_label' =>   '(en_CA) Verify Your Email Address',
    'email_verify_text' =>   '(en_CA) Before proceeding, please check your email for a verification link.',
    'email_verify_recieve_text' =>   '(en_CA) If you did not receive the email,',
    'email_verify_link' =>   '(en_CA) click here to request another.',
    'email_verify_resend_label' =>   '(en_CA) A fresh verification link has been sent to your email address.',
];
