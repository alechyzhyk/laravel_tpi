<?php

return [
    'index' => '(fr_FR) Accueil',
    'login' => 'Connexion',
    'register' => 'Créer un compte',
    'logout' => 'Deconnexion',
    'stats' => 'Statistiques',
    'bookmarks' => 'Favoris',
    'dashboard' => 'Tableau de bord',
    'dashboard_adm' => 'Tableau de bord admin',
    'manage_users' => 'Gestion utilisateurs',
    'manage_websites' => 'Gestion sites web',
    'manage_profile' => 'Gestion profil',
];
