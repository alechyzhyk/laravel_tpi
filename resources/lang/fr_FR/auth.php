<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '(fr_FR) These credentials do not match our records.',
    'password' => '(fr_FR) The provided password is incorrect.',
    'throttle' => '(fr_FR) Too many login attempts. Please try again in :seconds seconds.',
    'email_label' => '(fr_FR) Email',
    'password_label' => '(fr_FR) Password',
    'password_confirm_label' => '(fr_FR) Confirm password',
    'reset' => '(fr_FR) Reset password',
    'remember' => '(fr_FR) Remeber me',
    'no_account' => "(fr_FR) Don't have an account?",
    'firstname_label' => '(fr_FR) Firstname',
    'lastname_label' => '(fr_FR) Lastname',
    'birthdate_label' => '(fr_FR) Birthdate',
    'have_account' => '(fr_FR) Already have an account?',
    'email_verify_label' =>   '(fr_FR) Verify Your Email Address',
    'email_verify_text' =>   '(fr_FR) Before proceeding, please check your email for a verification link.',
    'email_verify_recieve_text' =>   '(fr_FR) If you did not receive the email,',
    'email_verify_link' =>   '(fr_FR) click here to request another.',
    'email_verify_resend_label' =>   '(fr_FR) A fresh verification link has been sent to your email address.',
];
