<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '(fr_CH) These credentials do not match our records.',
    'password' => '(fr_CH) The provided password is incorrect.',
    'throttle' => '(fr_CH) Too many login attempts. Please try again in :seconds seconds.',
    'email_label' => '(fr_CH) Email',
    'password_label' => '(fr_CH) Password',
    'password_confirm_label' => '(fr_CH) Confirm password',
    'reset' => '(fr_CH) Reset password',
    'remember' => '(fr_CH) Remeber me',
    'no_account' => "(fr_CH) Don't have an account?",
    'firstname_label' => '(fr_CH) Firstname',
    'lastname_label' => '(fr_CH) Lastname',
    'birthdate_label' => '(fr_CH) Birthdate',
    'have_account' => '(fr_CH) Already have an account?',
    'email_verify_label' =>   '(fr_CH) Verify Your Email Address',
    'email_verify_text' =>   '(fr_CH) Before proceeding, please check your email for a verification link.',
    'email_verify_recieve_text' =>   '(fr_CH) If you did not receive the email,',
    'email_verify_link' =>   '(fr_CH) click here to request another.',
    'email_verify_resend_label' =>   '(fr_CH) A fresh verification link has been sent to your email address.',
];
