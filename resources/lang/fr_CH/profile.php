<?php

return [
    'update_title' => 'Modifier les données',
    'change_pwd_label' => 'Changer le mot de passe',
    'update_label' => 'Mettre à jour',
    'update_pwd_label' => 'Mettre à jour',
    'update_pwd_title' => 'Changer le mot de passe',
    'curr_pwd_label' => 'Mot de passe actuel',
    'new_pwd_label' => 'Nouveau mot de passe',
    'confirm_pwd_label' => 'Confirmer mot de passe',
];
